
import {
  dbSearch,
  dbSearchCallback,
  dbSearchDelayed,
  dbSearchPromise,
  timeMessage,
  dbSearchPromiseReject
} from "./js/dbSearch.js"

// console.log("search person 1",dbSearch(1));
// console.log("search person -1",dbSearch(-1));
//
function moemoe(id) {
  let person = dbSearch(id);
  let mama = dbSearch(person.mother);
  return dbSearch(mama.mother);
}

// console.log(moemoe(1))

//console.log("Delayed search",db.timeMessage(db.dbSearchDelayed(1)+""))

// dbSearchCallback(2,console.log)
// console.log(timeMessage("finidhed searching mama of 2"));

// dbSearchCallback(2,result => {
//   dbSearchCallback(result.mother,console.log);
// })
//
// dbSearchCallback(6,result => {
//   dbSearchCallback(result.mother,result => {
//     dbSearchCallback(result.mother,console.log);
//   });
// })

// dbSearchCallback(6,result => {
//   dbSearchCallback(result.mother,result => {
//     dbSearchCallback(result.mother,result => {
//       dbSearchCallback(result.mother,console.log);
//     });
//   });
// })

// dbSearchPromise(6)
//   .then(console.log)
// console.log(timeMessage("finidhed searching  6"));

// mama
// dbSearchPromise(6)
//   .then(person => dbSearchPromise(person.mother))
//   .then(console.log);

dbSearchPromise(2)
  .then(person => dbSearchPromiseReject(person.mother))
  .then(person => dbSearchPromiseReject(person.mother))
  .then(person => dbSearchPromiseReject(person.mother))
  .then(console.log)
  .catch(err => console.log("kan overgrootmoeder niet vinden!", err));

// dbSearchPromise(2)
//   .then(person => dbSearchPromise(person.mother))
//   .then(person => dbSearchPromise(person.mother))
//   .then(person => dbSearchPromise(person.mother))
//   .then(console.log,err => console.log("kan overgrootmoeder niet vinden!", err));

// async function moemoeAsync(id){
//   let person = await db.dbSearchPromise(id);
//   let ma = await db.dbSearchPromise(person.mother)
//   return await db.dbSearchPromise(ma.mother)
// }

// moemoeAsync(1)
// console.log(timeMessage("finidhed searching  1"));
//moemoeAsync(1).then(console.log)
//
// let onsMoemoe = await moemoeAsync(1)
// console.log(timeMessage("Ons moemoe is "),onsMoemoe)


