export const db = [{id: 1, born: '1966', name: 'Rika', mother: 2},
  {id: 2, born: '1942', name: 'Gerda', mother: 3},
  {id: 3, born: '1925', name: 'Stefanie', mother: 0},
  {id: 4, born: '1949', name: 'Mia', mother: 3},
  {id: 5, born: '1967', name: 'Ineke', mother: 2},
  {id: 6, born: '1994', name: 'Hanna', mother: 5}
];
