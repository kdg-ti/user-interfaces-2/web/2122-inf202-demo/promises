/** Created by Jan de Rijke */

// start timer is done when loading the module for simplicity
// a goood implementation would have a start method to be called by the user of the module
import  {db} from "./db.js"
import timers from "timers/promises"
const start = new Date()


function getPerson(id) {
  return db.find(person => person.id === id)
}

export function timeMessage (msg){
  return `${new Date() - start} ms: ${JSON.stringify(msg)}`
}

function indentTimeMessage(msg){
  return "\t>" + timeMessage(msg)
}

function dbLog(id, result) {
  console.log(indentTimeMessage(result ? result : "Invalid ID: " + id));
}

export function dbSearch(id) {
  let person= getPerson(id);
  dbLog(id,person)
  return person
}

export function dbSearchDelayed(id){
  return setTimeout(() => dbSearch(id),Math.random()*1000)
}

export function dbSearchCallback(id,callback){
  setTimeout(() => callback(dbSearch(id)),Math.random()*1000)
}

export function dbSearchPromise(id){
  return timers.setTimeout(Math.random()*1000)
    .then(() => dbSearch(id))
}

export function dbSearchPromiseReject(id){
  return timers.setTimeout(Math.random()*1000)
    .then(() => dbSearch(id))
    .then(result => result??Promise.reject("Rejecting id " + id))
      // dit kan ook: throw new Error("Invalid id"))
}




